var fs = require('fs');
var wd = require('wd');
var fork = require('child_process').fork

var WebDriverInstance = function (baseBrowserDecorator) {
  var self = this;
  baseBrowserDecorator(this);

  this._start = function (url) {
    console.log([self.WEBDRIVER_URL, self.WEBDRIVER_PORT, self.WEBDRIVER_USER, self.WEBDRIVER_APIKEY].join('\n'));
    self.browser = wd.remote(self.WEBDRIVER_URL, self.WEBDRIVER_PORT, self.WEBDRIVER_USER, self.WEBDRIVER_APIKEY);

    self.browser.init({
      browserName: 'chrome'
    }, function () {
      console.log('WebDriver callback fired');
      self.browser.get(url);
    });

    console.log('WebDriver started');
  };
};

WebDriverInstance.prototype = {
  name: 'WebDriver',

  DEFAULT_CMD: {
    linux: require('wd').path,
    darwin: require('wd').path,
    win32: require('wd').path
  },
  ENV_CMD: 'WEBDRIVER_BIN',

  WEBDRIVER_URL: 'ondemand.saucelabs.com',
  WEBDRIVER_PORT: 80,
  WEBDRIVER_USER: 'jbastow',
  WEBDRIVER_APIKEY: '5d98c646-0f32-4e98-861c-85dc3dc8eaf4'
};

WebDriverInstance.$inject = ['baseBrowserDecorator'];


// PUBLISH DI MODULE
module.exports = {
  'launcher:WebDriver': ['type', WebDriverInstance]
};